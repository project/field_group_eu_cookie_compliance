CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Field Group EU Cookie Compliance module allows to hide fields, if the cookie
category is not accepted.


 * For a full description of the module visit:
   https://www.drupal.org/project/field_group_eu_cookie_compliance

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/field_group_eu_cookie_compliance


REQUIREMENTS
------------

This module requires the Field Group and the EU Cookie Compliance module.
Internal Page Cache module has to be disabled. Cookies cache context has to be
added.


INSTALLATION
------------

Install the Field Group EU Cookie Compliance module as you would normally 
install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 
for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Field Group EU Cookie
       Compliance module.
    2. Use field group in content display.
    3. Disable Internal Page Cache module.
    4. Add 'cookies:cookie-agreed-categories' as required_cache_contexts in
       sites/default/services.yml: see services_example.yml file


TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
