<?php

namespace Drupal\field_group_eu_cookie_compliance\Plugin\field_group\FieldGroupFormatter;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Utility\Token;
use Drupal\field_group\FieldGroupFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldGroupFormatter(
 *   id = "eucookiecompliance",
 *   label = @Translation("EU Cookie Compliance"),
 *   description = @Translation("Hide field group if cookie category is not accepted."),
 *   supported_contexts = {
 *     "view",
 *   },
 *   supported_link_field_types = {
 *    "link",
 *    "entity_reference",
 *    "file",
 *    "image",
 *   }
 * )
 */
class EuCookieCompliance extends FieldGroupFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['group'],
      $configuration['settings'],
      $configuration['label'],
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('module_handler'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, $group, array $settings, $label, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ModuleHandlerInterface $module_handler, Token $token) {
    parent::__construct($plugin_id, $plugin_definition, $group, $settings, $label);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->moduleHandler = $module_handler;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultContextSettings($context) {
    $defaults = [
      'cookie_category' => '',
    ] + parent::defaultSettings();

    if ($context == 'form') {
      $defaults['required_fields'] = 1;
    }

    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {

    $form = parent::settingsForm();

    $categories = [];
    /** @var \Drupal\eu_cookie_compliance\CategoryStorageManager $categoryStorageManager */
    $categoryStorageManager = $this->entityTypeManager->getStorage('cookie_category');
    foreach ($categoryStorageManager->getCookieCategories() as $category) {
      $categories[$category['id']] = $category['label'];
    }

    $form['cookie_category'] = [
      '#title' => $this->t('Cookie category'),
      '#description' => $this->t('Nessessary cookie category to display fields.'),
      '#type' => 'select',
      '#options' => $categories,
      '#default_value' => $this->getSetting('cookie_category'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$element, $rendering_object) {
    $cookie = \Drupal::request()->cookies->get('cookie-agreed-categories');
    $element['#cache']['contexts'][] = 'cookies:cookie-agreed-categories';
    $element['#cache']['tags']['cookies:cookie-agreed-categories'] = $cookie;
    $element['#cache']['max-age'] = 0;
    if ($cookie === NULL || !in_array($this->getSetting('cookie_category'), json_decode($cookie))) {
      foreach (Element::children($element) as $group_child) {
        unset($element[$group_child]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getClasses() {
    $classes = ['field-group-eu-cookie-compliance'];
    $classes = array_merge($classes, parent::getClasses());
    return $classes;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Cookie category :category', [':category' => ': ' . $this->getSetting('cookie_category')]);
    return $summary;
  }

}
